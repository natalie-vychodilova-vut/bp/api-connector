import rel as rel
import websocket
import json

DEST_URI = "ws://demo.sewio.net:8080"
X_API_KEY = "17254faec6a60f58458308763"


def on_message(ws, message):
    data = json.loads(message)
    # manage_data(data["body"])
    print(data)


def on_error(ws, error):
    print(error)


def on_close(ws, close_status_code, close_msg):
    print("### closed ###")


def on_open(ws):
    print("### opened ###")
    messages = [
        {
            "headers": {"X-ApiKey": X_API_KEY},
            "method": "subscribe",
            "resource": "/feeds/"},
        {
            "headers": {"X-ApiKey": X_API_KEY},
            "method": "subscribe",
            "resource": "/restrictions/"},
        {
            "headers": {"X-ApiKey": X_API_KEY},
            "method": "subscribe",
            "resource": "/zones/"}
    ]

    print("Sending message...")

    print(json.dumps(messages[2]))
    ws.send(json.dumps(messages[2]))


if __name__ == "__main__":
    #websocket.enableTrace(True)
    ws = websocket.WebSocketApp(DEST_URI, on_open=on_open, on_message=on_message, on_error=on_error, on_close=on_close)
    ws.run_forever()
    #ws.run_forever(dispatcher=rel, reconnect=5)
    #rel.signal(2, rel.abort)
    #rel.dispatch()
