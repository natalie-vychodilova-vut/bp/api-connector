from datetime import datetime, timedelta

time_in_str = "2023-02-15 17:18:30.177360"
time_out_str = "2023-02-15 17:13:10.177360"

time_in = datetime.strptime(time_in_str, "%Y-%m-%d %H:%M:%S.%f")
time_out = datetime.strptime(time_out_str, "%Y-%m-%d %H:%M:%S.%f")


def is_end(time_enter, seconds: int) -> bool:
    now = datetime.now()
    timeout = time_enter + timedelta(seconds=seconds)

    if timeout >= now:
        return False
    else:
        return True


if __name__ == '__main__':

    while True:
        if is_end(time_in, 10):
            print("Ted")
