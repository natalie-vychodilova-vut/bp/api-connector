from src.classes.zone import Zone
from src.classes.tag import Tag

if __name__ == '__main__':
    tag1 = Tag(1)
    tag2 = Tag(2)

    pseudo1 = Tag(1)

    zone = Zone(6)

    zone.add(tag1)
    zone.add(tag2)

    print(zone)

    zone.add(pseudo1)

    print(zone)

